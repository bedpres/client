<?php

namespace BPC;

interface Connection {
	public function __construct(Config $config);
	public function request($type, $request);
}