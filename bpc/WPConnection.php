<?php

namespace BPC;

/**
 * Class WPConnection
 *
 * @package BPC
 */
class WPConnection implements Connection {
	/** @var Config $config */
	public $config;

	private $handle;

	/**
	 * @param Config $config A BPC config object
	 */
	function __construct(Config $config) {
		if(!function_exists('add_action')) {
			trigger_error("Wordpress is not loaded", E_USER_ERROR);
		}
		$this->config = $config;
	}

	/**
	 * request
	 * Perform a request to the API using the WordPress stream API.
	 *
	 * @param string $type     The request type
	 * @param array  $request  An array describing the request parameters
	 * @return array
	 */
	public function request($type, $request) {
		$conf = $this->config->getConfig();
		$url = $this->config->getUrl();

		$request = array_merge($conf, $request);
		$request['request'] = $type;

		$response = wp_remote_post( $url, array(
						'method' => 'POST',
						'timeout' => 2,
						'redirection' => 5,
						'httpversion' => '1.0',
						'blocking' => true,
						'headers' => array(),
						'body' => $request,
						'cookies' => array()
				)
		);

		if(!is_wp_error($response)) {
			$data = json_decode($response['body'], TRUE);
		} else {
			$data = FALSE;
		}

		if(!$data) {
			$data = array('error' => array(0 => array(100 => 'Malformed reply from remote server.')));
		}
		return $data;
	}
}