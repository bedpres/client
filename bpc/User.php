<?php

namespace BPC;

/**
 * Class User
 *
 * @package BPC
 */
class User {
	public $username;
	public $name;
	public $cardhash;
	public $year;

	/**
	 * @param string $username  NTNU username
	 * @param string $name      Full name
	 * @param string $cardhash  sha1 hash of the card number (take care to strip all leading zeros, e.g. by casting to int)
	 * @param int $year      The cohort of the user, typically between 1 and 5
	 */
	function __construct($username, $name, $cardhash, $year) {
		$this->username = $username;
		$this->name = $name;
		$this->cardhash = $cardhash;
		$this->year = $year;
	}
}