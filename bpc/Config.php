<?php

namespace BPC;

/**
 * Class Config
 *
 * @package BPC
 */
class Config {
	private $data = array();
	private $url = NULL;

	/**
	 * @param int    $forening  API identity
	 * @param string $key       API key
	 * @param bool   $testing   Whether to talk to the testing environment
	 */
	public function __construct($forening, $key, $testing = FALSE) {
		$this->data = array(
			'forening' => $forening,
			'key' => $key,
			'version' => '1.7',
			'method' => 'json'
		);

		if ($testing) {
			$this->url = 'http://testing.bedriftspresentasjon.no/remote/';
		} else {
			$this->url = 'https://www.bedriftspresentasjon.no/remote/';
		}
	}

	public function __get($key) {
		if (array_key_exists($key, $this->data)) {
			return $this->data[$key];
		}
		trigger_error("Undefined config property $key", E_USER_ERROR);
	}

	public function __set($key, $val) {
		if (array_key_exists($key, $this->data)) {
			$this->data[$key] = (string)$val;
		} else {
			trigger_error("Undefined config property $key. Value was not stored.", E_USER_ERROR);
		}
	}

	/**
	 * getConfig
	 * Get the current config.
	 *
	 * @return array
	 */
	public function getConfig() {
		if(isset($this->data['forening'])) {
			return $this->data;
		}
	}

	/**
	 * getUrl
	 * Get the URL to the API
	 *
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}
}