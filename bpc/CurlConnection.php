<?php

namespace BPC;

/**
 * Class CurlConnection
 *
 * @package BPC
 */
class CurlConnection implements Connection {
	/** @var Config $config */
	public $config;

	private $handle;

	/**
	 * @param Config $config A BPC config object
	 */
	function __construct(Config $config) {
		if(!function_exists('curl_init')) {
			trigger_error("cURL is not installed", E_USER_ERROR);
		}
		$this->config = $config;
		$this->handle = curl_init();
	}

	function __destruct() {
		curl_close($this->handle);
	}

	/**
	 * request
	 * Perform a cURL request to the API.
	 *
	 * @param string $type     The request type
	 * @param array  $request  An array describing the request parameters
	 * @return array
	 */
	public function request($type, $request) {
		$conf = $this->config->getConfig();
		$url = $this->config->getUrl();

		$request = array_merge($conf, $request);
		$request['request'] = $type;

		// Set curl options
		curl_setopt_array($this->handle, array(
				CURLOPT_URL => $url,
				CURLOPT_POSTFIELDS => $request,
				CURLOPT_SSL_VERIFYPEER => FALSE,
				CURLOPT_RETURNTRANSFER => TRUE,
				CURLOPT_HEADER => FALSE,
				CURLOPT_FOLLOWLOCATION => TRUE,
				CURLOPT_CONNECTTIMEOUT => 1
		));

		$data = curl_exec($this->handle);
		$data = json_decode($data, TRUE);
		if(!$data) {
			$data = array('error' => array(0 => array(100 => 'Malformed reply from remote server.')));
		}
		return $data;
	}
}