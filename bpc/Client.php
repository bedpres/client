<?php
namespace BPC;

const EVENT_TYPE_ADVERTISED = 'advertised';
const EVENT_TYPE_NOT_ADVERTISED = 'not_advertised';
const EVENT_TYPE_ALL = 'all';

const SORT_LASTNAME = 'lname';
const SORT_FIRSTNAME = 'fname';
const SORT_USERNAME = 'username';
const SORT_REGISTERED = 'registered';

/**
 * Class Client
 *
 * @package BPC
 */
class Client {
	/** @var Connection $conn */
	private $conn;

	/** @var User $user */
	public $user;
	private $errors = NULL;

	/**
	 * @param Connection $conn A BPC connection object (e.g CurlConnection or WPConnection)
	 * @param User       $user A BPC user object
	 */
	public function __construct(Connection $conn, User $user) {
		$this->conn = $conn;
		$this->user = $user;
	}

	/**
	 * getEvents
	 * Get all events between two dates of a given type
	 *
	 * @param string|null   $from	A strtotime-compatible string describing the start of the range to fetch events. Default: current time
	 * @param string|null   $to   A strtotime-compatible string describing the end of the range to fetch events. Default: heat death of the universe
	 * @param string        $type The event type. Should be one of \BPC\EVENT_TYPE_ADVERTISED, \BPC\EVENT_TYPE_NOT_ADVERTISED, \BPC\EVENT_TYPE_ALL. Default: \BPC\EVENT_TYPE_ADVERTISED
	 *
	 * @return array|bool         An array of events (each event is described by an array). Empty array if no events. FALSE if an error occurred.
	 */
	public function getEvents($from = NULL, $to = NULL, $type = EVENT_TYPE_ADVERTISED) {
		$params = array(
			'event_type' => $type,
			'username' => $this->user->username
		);
		if(isset($from)) {
			$params['fromdate'] = $from;
		}
		if(isset($to)) {
			$params['todate'] = $to;
		}

		$reply = $this->conn->request('get_events', $params);
		if($this->handleErrors($reply, array(403))) {
			if(isset($reply['event'])) {
				return $reply['event'];
			} else {
				return array();
			}
		} else {
			return FALSE;
		}
	}

	/**
	 * getEvent
	 * Get a single event of a given type
	 *
	 * @param int    $eid   The event id.
	 * @param string $type  The event type. Should be one of \BPC\EVENT_TYPE_ADVERTISED, \BPC\EVENT_TYPE_NOT_ADVERTISED, \BPC\EVENT_TYPE_ALL. Default: \BPC\EVENT_TYPE_ADVERTISED
	 * @return array|bool   An array describing the event. FALSE if an error occurred (e.g. if the event with the given id does not exist)
	 */
	public function getEvent($eid, $type = EVENT_TYPE_ADVERTISED) {
		$params = array(
			'username' => $this->user->username,
			'event_type' => $type,
			'event' => $eid
		);

		$reply = $this->conn->request('get_events', $params);
		if($this->handleErrors($reply)) {
			return $reply['event'][0];
		} else {
			return FALSE;
		}
	}

	/**
	 * getAttendingUsers
	 * Get all users attending an event
	 *
	 * @param int    $eid   The event id.
	 * @param string $sort  How to sort the user list. Should be one of \BPC\SORT_LASTNAME, \BPC\SORT_FIRSTNAME, \BPC\SORT_REGISTERED.
	 * @return array|bool   An array describing the list of attending users. Empty array if no users attending. FALSE if an error occurred.
	 */
	public function getAttendingUsers($eid, $sort = SORT_LASTNAME) {
		return $this->_getUsers('get_attending', $eid, $sort);
	}

	/**
	 * getWaitingUsers
	 * Get all users on the waitlist for an event
	 *
	 * @param int    $eid   The event id.
	 * @param string $sort  How to sort the user list. Should be one of \BPC\SORT_LASTNAME, \BPC\SORT_FIRSTNAME, \BPC\SORT_REGISTERED.
	 * @return array|bool   An array describing the list of attending users. Empty array if no users waiting. FALSE if an error occurred.
	 */
	public function getWaitingUsers($eid, $sort = SORT_REGISTERED) {
		return $this->_getUsers('get_waiting', $eid, $sort);
	}

	private function _getUsers($request, $eid, $sort) {
		$params = array(
				'event' => $eid,
				'sort' => $sort
		);

		$reply = $this->conn->request($request, $params);
		if($this->handleErrors($reply, array(404))) {
			if(isset($reply['users'])) {
				return $reply['users'];
			} else {
				return array();
			}
		} else {
			return FALSE;
		}
	}

	/**
	 * registerUser
	 * Register the user as attending an event
	 *
	 * @param int $eid    The event id.
	 * @return bool|array TRUE if the user was registered. Array on the form ['waiting' => int] if put on waitlist. FALSE if an error occurred.
	 */
	public function registerUser($eid) {
		$params = array(
			'fullname' => $this->user->name,
			'username' => $this->user->username,
			'card_no' => $this->user->cardhash,
			'year' => $this->user->year,
			'event' => $eid
		);

		$reply = $this->conn->request('add_attending', $params);
		if($this->handleErrors($reply)) {
			if(isset($reply['add_attending'])) {
				if($reply['add_attending'][0] === '1') {
					return TRUE;
				} else {
					return $reply['add_attending'][0];
				}
			}
		}
		return FALSE;
	}

	/**
	 * deregisterUser
	 * Deregister the user from an event. (No longer attending)
	 *
	 * @param int $eid  The event id.
	 * @return bool     TRUE if the user was deregistered. FALSE if an error occurred.
	 */
	public function deregisterUser($eid) {
		$params = array(
			'username' => $this->user->username,
			'event' => $eid
		);

		$reply = $this->conn->request('rem_attending', $params);
		if($this->handleErrors($reply)) {
			if(isset($reply['rem_attending'])) {
				return $reply['rem_attending'][0] === '1';
			}
		}
		return FALSE;
	}

	/**
	 * getAttendanceStatistics
	 * Get statistics on how many events a user attended, did not show up for, or was on the waitlist for in a timespan.
	 * @param string   $from A strtotime-compatible string describing the start of the range to calculate statistics. Default: fall of the roman empire
	 * @param string   $to   A strtotime-compatible string describing the end of the range to calculate statistics. Default: current time
	 * @param string $type   The event type. Should be one of \BPC\EVENT_TYPE_ADVERTISED, \BPC\EVENT_TYPE_NOT_ADVERTISED, \BPC\EVENT_TYPE_ALL. Default: \BPC\EVENT_TYPE_ADVERTISED
	 * @return array|bool    Array describing statistics. FALSE if an error occurred.
	 */
	public function getAttendanceStatistics($from = NULL, $to = NULL, $type = EVENT_TYPE_ADVERTISED) {
		$params = array(
			'username' => $this->user->username,
			'event_type' => $type
		);
		if(isset($from)) {
			$params['fromdate'] = strftime('%Y-%m-%d %H:%M:%S', strtotime($from));
		}
		if(isset($to)) {
			$params['todate'] = strftime('%Y-%m-%d %H:%M:%S', strtotime($to));
		}

		$reply = $this->conn->request('get_user_stats', $params);
		if($this->handleErrors($reply)) {
			return $reply['user_stats'][0];
		}
		return FALSE;
	}

	private function handleErrors($data, $ignore = array()) {
		if(!isset($data['error'])) {
			return TRUE;
		}
		$errors = array();
		foreach($data['error'] as $error) {
			foreach($error as $code => $desc) {
				if(!in_array($code, $ignore)) {
					$errors[$code] = $desc;
				}
			}
		}

		if(!empty($errors)) {
			$this->errors = $errors;
			return FALSE;
		} else {
			return TRUE;
		}
	}

	/**
	 * getLastError
	 * Get all errors from the last communication with the API.
	 *
	 * @return array|null Array describing errors from last communication with the API. NULL if no errors occurred.
	 */
	public function getLastError() {
		$last_error = $this->errors;
		$this->errors = NULL;

		return $last_error;
	}
}