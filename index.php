<?php

include('autoload.php');

/* Create a new config object.
 * id: The id of the association
 * key: the API key
 * testing: whether we should talk to the test environment or not (i.e. the production environment)
 */
$conf = new \BPC\Config('id', 'key', TRUE);

/* Use the config to create a connection. There are two sample connection objects: cURL and WordPress */
$conn = new \BPC\CurlConnection($conf);

/* Create the current user with data. This would be pulled from the profile of your website.
 * username: NTNU username
 * name: Full name
 * cardhash: sha1 hash of the integer form of the card number (i.e. no leading zeros)
 * year: The the cohort of the user (typically in the range 1 through 5)
 */
$user = new \BPC\User('nordmann', 'Ola Nordmann', sha1('1234'), 5);

/* Create a client! */
$bpc = new \BPC\Client($conn, $user);

/* Talk to the client. */

/* Get all advertised events in the future */
var_dump($bpc->getEvents());

/* Get all advertised events next month (30 days) */
var_dump($bpc->getEvents(NULL, '+1 month'));

/* Get all events between two dates */
var_dump($bpc->getEvents('2015-01-01', '2015-02-01'));

/* If a query returned FALSE, we should check the error output */
var_dump($bpc->getLastError());